import logging
from pathlib import Path
from github import Github, GithubException, Repository
from lib.downloader import RepoDownloader, VersionControlType


logger = logging.getLogger("RepoDownloader.GitHub")


class GithubDownloader(RepoDownloader):

    repositories: list[Repository]

    def __init__(self, username: str, access_token: str, download_dir: str | Path):
        super().__init__(username, access_token, download_dir)
        self.version_control = VersionControlType.GITHUB
        self.download_dir = self.build_download_dir()


        # Creating GitHub object with given token
        if self.access_token:
            logger.debug("Token provided. Attempting to list all repos.")
            self.github = Github(self.username, self.access_token)
        else:
            logger.warning("No token provided. Only public repos.")
            self.github = Github(self.username)

    def get_repositories(self):
        try:
            self.repositories = [r for r in self.github.get_user().get_repos()]
        except GithubException as e:
            logger.error(e)

    def download(self):
        logger.info("#" * 80)

        # Fetching list of GitHub repositories
        logger.info("## Fetching repos list from GitHub ##")
        self.get_repositories()
        if not self.repositories:
            logger.warning("Repository list empty. Exiting")
            exit(0)

        logger.info(f"Number of repositories found: {len(self.repositories)}")
        if len(self.repositories) > 0:
            # Create archive dir
            self.create_path(self.download_dir)

            # Clone repositories and archive
            logger.info("## Downloading repositories ##")
            for repository in self.repositories:
                logger.info(f"Saving {repository.full_name}...")
                clone_path = Path(self.download_dir, repository.full_name)
                self.clone(repository.clone_url, clone_path)
                self.create_zip_archive(f"{str(clone_path.resolve())}", str(clone_path.resolve()), delete_dir=True)
        else:
            logger.warning("Repository list empty. Exiting")
        logger.info("#" * 80)
