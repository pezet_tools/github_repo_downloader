import os
import shutil
import logging
from abc import ABC
from enum import Enum
from pathlib import Path
from datetime import datetime


logger = logging.getLogger("RepoDownloader")


class VersionControlType(Enum):
    GITLAB = 'gitlab'
    GITHUB = 'github'


class RepoDownloader(ABC):

    version_control: VersionControlType

    def __init__(self, username: str, access_token: str, download_dir: str | Path):
        self.username = username
        self.access_token = access_token
        self.download_dir = download_dir

    def build_download_dir(self):
        return Path(self.download_dir, f"{self.version_control.value}_{datetime.now().strftime('%Y%m%d_%H%M%S')}")

    def clone(self, clone_url: str, clone_dir: str | Path):
        self.create_path(clone_dir)
        clone_url_array = clone_url.split("//")
        if logger.level == logging.DEBUG:
            quiet = ""
        else:
            quiet = "-q "
        command = f"git clone {quiet}{clone_url_array[0]}//{self.username}:{self.access_token}@{clone_url_array[1]} \"{clone_dir}\""
        logger.debug(f"Cloning command: '''{command}'''")
        return_code = os.system(command)
        logger.debug(f"{return_code=}")

    @staticmethod
    def create_path(*elements: str | Path, is_file: bool = False):
        p = Path(*elements)
        if is_file:
            logger.debug(f"Creating a file: {p}...")
            p.touch(exist_ok=True)
        else:
            logger.debug(f"Creating a directory: {p}...")
            p.mkdir(parents=True, exist_ok=True)

    @staticmethod
    def delete_path(file_name: str | Path):
        p = Path(file_name)
        if p.is_file():
            logger.debug(f"Deleting path: {file_name}...")
            p.unlink(missing_ok=True)
        elif p.is_dir():
            shutil.rmtree(p, ignore_errors=True)

    @staticmethod
    def create_zip_archive(output_filename: str, dir_to_archive: str, delete_dir: bool = False):
        logger.debug(f"{dir_to_archive=}")
        logger.debug(f"{output_filename=}")
        logger.debug(f"{delete_dir=}")
        shutil.make_archive(output_filename, 'zip', dir_to_archive)
        if delete_dir:
            shutil.rmtree(dir_to_archive, ignore_errors=False, onerror=onerror)


def onerror(func, path, exc_info):
    """
    Error handler for ``shutil.rmtree``.
    If the error is due to an access error (read only file)
    it attempts to add write permission and then retries.
    If the error is for another reason it re-raises the error.
    Usage : ``shutil.rmtree(path, onerror=onerror)``
    """
    import stat
    # Is the error an access error?
    if not os.access(path, os.W_OK):
        os.chmod(path, stat.S_IWUSR)
        func(path)
    else:
        raise
