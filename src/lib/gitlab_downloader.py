from pathlib import Path
import httpx
import logging

from lib.downloader import RepoDownloader, VersionControlType


logger = logging.getLogger("RepoDownloader.GitLab")


class Project:

    def __init__(self, p_id: int, p_name: str, p_group_name: str, clone_url: str):
        self.p_id = p_id
        self.p_name = p_name
        self.p_group_name = p_group_name
        self.clone_url = clone_url

    def __repr__(self) -> str:
        return f"id: {self.p_id}, name: {self.p_name}, group_name: {self.p_group_name} clone_url: {self.clone_url}"


class Gitlab:

    def __init__(self, username: str, api_url: str, access_token: str = None):
        self.username = username
        self.api_url = api_url
        self.access_token = access_token
        self.headers = {"PRIVATE-TOKEN": self.access_token}

    def __make_get(self, endpoint: str, headers: dict = None) -> httpx.Response:
        url = f"{self.api_url}{endpoint}"
        logger.debug(f"Api GET: {url}")
        _headers = self.headers
        if headers:
            _headers.update(headers)
        response = httpx.get(url, headers=_headers)
        if logger.level == logging.DEBUG:
            logger.debug(f"Response: {response} *** {response.text}")
        return response

    def get_groups(self) -> dict:
        groups = dict()
        response = self.__make_get(endpoint="/groups").json()
        if response:
            groups = {_group.get('id'): _group.get('name') for _group in response}
            logger.debug(f"groups: {groups}")
        if not groups:
            logger.warning("Groups list empty. Exiting")
            exit(1)
        logger.info(f"Found {len(groups)} groups.")
        return groups

    def get_group_projects(self, group_id: int) -> list[Project]:
        response = self.__make_get(endpoint=f"/groups/{group_id}").json()
        projects = list()
        if response:
            raw_projects = response.get("projects", [])
            for _proj in raw_projects:
                projects.append(Project(_proj.get('id'),
                                        _proj.get('name'),
                                        _proj.get('namespace').get('name'),
                                        _proj.get('http_url_to_repo'),
                                        ))
            logger.debug(f"projects: \n{projects}")
            logger.info(f"Found {len(projects)} projects.")
            return projects


class GitlabDownloader(RepoDownloader):

    def __init__(self, username: str, access_token: str, download_dir: str | Path, api_url: str):
        super().__init__(username, access_token, download_dir)
        self.version_control = VersionControlType.GITLAB
        self.download_dir = self.build_download_dir()
        self.projects: list[Project] = list()

        # Creating GitLab object with given token
        if self.access_token:
            logger.debug("Token provided. Attempting to list all projects.")
            self.gitlab = Gitlab(self.username, api_url, self.access_token)
        else:
            logger.warning("No token provided. Only public projects.")
            self.gitlab = Gitlab(self.username, api_url)


    def get_projects(self, groups: dict):
        if groups:
            for group_id, group_name in groups.items():
                self.projects.extend(self.gitlab.get_group_projects(group_id))
        if not self.projects:
            logger.warning("Project list empty. Exiting")
            exit(0)
        logger.info(f"## Across all groups found {len(self.projects)} projects.")


    def download(self):
        logger.info("#" * 80)

        # Fetching user groups
        logger.info("## Fetching group list ##")
        groups = self.gitlab.get_groups()

        # Fetching projects for repositories
        logger.info("## Fetching project list ##")
        self.get_projects(groups)

        # Cloning repositories
        if not self.projects or len(self.projects) == 0:
            logger.warning("Project list empty. Exiting")
            exit(0)

        # Create base archive dir
        self.create_path(self.download_dir)

        # Clone repositories and archive
        logger.info("## Downloading projects ##")
        for project in self.projects:
            logger.info(f"Saving {project.p_group_name}/{project.p_name}...")
            clone_path = Path(self.download_dir, project.p_group_name, project.p_name)
            self.clone(project.clone_url, clone_path)
            self.create_zip_archive(f"{str(clone_path.resolve())}", str(clone_path.resolve()), delete_dir=True)
