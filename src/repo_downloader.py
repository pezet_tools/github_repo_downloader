import os
from pathlib import Path
import logging
import argparse
from datetime import datetime
from dotenv import load_dotenv
from lib.github_downloader import GithubDownloader
from lib.gitlab_downloader import GitlabDownloader


REPOSITORY_GITHUB = 'github'
REPOSITORY_GITLAB = 'gitlab'
REPOSITORIES = [REPOSITORY_GITHUB, REPOSITORY_GITLAB]

logger = logging.getLogger("RepoDownloader")
FORMAT = '[%(asctime)s]: %(name)s %(levelname)s - %(message)s'
DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
logging.basicConfig(format=FORMAT, datefmt=DATE_FORMAT)
logger.setLevel(logging.INFO)


def parse_args():
    parser = argparse.ArgumentParser(description='Parse input parameters')

    parser.add_argument('-u', '--user',
                        required=True,
                        help='Repository username'
                        )
    parser.add_argument('-r', '--repository',
                        required=True,
                        choices=REPOSITORIES,
                        help='Specifies if this is github or gitlab repository'
                        )
    parser.add_argument('-d', '--directory',
                        help='Directory for downloaded content, by default it\'s scripts directory'
                        )
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='Debug mode'
                        )
    return parser.parse_args()


def build_dir_paths(main_dir: str, repository: str) -> dict:
    dirs: dict[str, str | Path] = dict()
    dirs['home'] = Path(main_dir) if main_dir else Path(Path(__file__).parent, "archive")
    dirs['download'] = Path(f"{dirs['home']}",
                                    f"{repository}_{datetime.now().strftime('%Y%m%d_%H%M%S')}")

    logger.info("home: ".rjust(10, ' ') + str(dirs['home'].resolve()))
    logger.info("download: ".rjust(10, ' ') + str(dirs['download'].resolve()))

    return dirs


def read_config() -> dict:
    config: dict[str, str] = dict()
    env_file = Path('.env')
    if not env_file.exists():
        logger.error("Token file empty. Exiting")
        exit(1)
    load_dotenv()
    config['GITHUB_RW'] = os.getenv('GITHUB_RW')
    config['GITLAB_RW'] = os.getenv('GITLAB_RW')
    config['GITLAB_API'] = os.getenv('GITLAB_API')
    return config


def main():
    args = parse_args()
    if args.repository not in REPOSITORIES:
        logger.error(f"ERROR: repository type {args.repository} not found. Expected values: {', '.join(REPOSITORIES)}")
        exit(1)

    if args.verbose:
        logger.setLevel(logging.DEBUG)

    logger.debug(args)

    logger.info("## Setting up necessary directories and files ##")
    download_directory = Path(args.directory) if args.directory else Path(Path(__file__).parent, "archive")

    if args.repository == REPOSITORY_GITHUB:
        access_token = read_config().get('GITHUB_RW', '')
        gd = GithubDownloader(args.user, access_token, download_directory)
    elif args.repository == REPOSITORY_GITLAB:
        _tokens = read_config()
        access_token = read_config().get('GITLAB_RW', '')
        api_url = read_config().get('GITLAB_API', '')
        gd = GitlabDownloader(args.user, access_token, download_directory, api_url)
    elif args.repository or not args.repository:
        logger.warning("Repository parameter not found. Exiting.")
        exit(1)

    gd.download()


if __name__ == "__main__":
    main()
