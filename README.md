# Repository downloader
Script that allows you to download all user repositories from:
- GitHub
- GitLab

Repositories are downloaded as bare files without commit history compressed to a zip archive.

## Basic info
- Default location is this app's directory unless specified otherwise.
- Token file contents should reflect following structure:
  ```
  github_rw=<token>
  gitlab_ro=<token>
  gitlab_rw=<token>
  ```

## Parameters
| parameters                                       | description                                                         |
|:-------------------------------------------------|:--------------------------------------------------------------------|
| ---h, --help                                     | Show this help message and exit                                     |
| -u USER, --user USER                             | Repository username                                                 |
| -t TOKEN_FILE, --token_file TOKEN_FILE           | Path to file with tokens                                            |
| -r {github,gitlab}, --repository {github,gitlab} | Specifies if this is github or gitlab repository                    |
| -ga GITLAB_API, --gitlab_api GITLAB_API          | Gitlab Api url                                                      |
| -d DIRECTORY, --directory DIRECTORY              | Directory for downloaded content, by default it's scripts directory |
| -v, --verbose                                    | Debug mode                                                          |

## Example 

- GitHub
  ```
  python3 ./repo_downloader.py -u <username> -r github -t /path/to/tokens/tokens.conf
  ```

- GitLab
  ```
  python3 ./repo_downloader.py -u <username> -r gitlab -t /path/to/tokens/tokens.conf --gitlab_api https://gitlab.com/api/v4
  ```